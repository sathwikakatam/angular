import { Component, OnInit,Input,Inject } from '@angular/core';
import { DishService } from '../Service/dish.service';
import { dishe } from '../Shared/dishes';
import { promotions } from '../Shared/Promotion';
import { PromotionService } from '../Service/promotion.service';
import {leader} from '../Shared/leader';
import { LEADER } from '../Shared/leaderData';
import {LeaderService} from '../Service/leader.service';
import { flyInout,expand } from '../Shared/animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  host:{
  '[@flyInout]':'true',
  'style':'display=block;'

  },
  animations:[
  flyInout(),
  expand()
  ]

})
export class HomeComponent implements OnInit {
 dish: dishes;
 H_lead:leader;
 errMss:string;
 promotion_F:promotions;

  constructor(private dishservice : DishService, private promotionservice:PromotionService,private leader: LeaderService,@Inject('BaseURL') private baseURL) { 
   }

  ngOnInit() {
  //console.log(this.baseURL);
  this.dishservice.getFeaturedDish().subscribe((F_dish)=> {this.dish =F_dish}, 
    errmess => { this.dish = null; this.dishcopy = null; this.errMss = <any>errmess;
    });
  this.leader.getFeatured().subscribe((f_leader)=>{this.H_lead=f_leader},
  errmess => { this.H_lead = null;
               this.errMss = <any>errmess
               });
  
  this.promotionservice.getFeaturedPromotion().subscribe((promotion)=>{this.promotion_F=promotion}, errmess => {         this.promotion = null;  this.errMss = <any>errmess 
});
  
  }


}
