import { Component, OnInit,Output,Inject } from '@angular/core';
import {LeaderService} from '../Service/leader.service';
import {leaders} from '../Shared/leader';
//import { LEADER } from '../Shared/leaderData';
import { flyInout,expand } from '../Shared/animations';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
  host:{
  '[@flyInout]':'true',
  'style':'display=block;'

  },
  animations:[
  flyInout(),
  expand()
  ],

})
export class AboutComponent implements OnInit {
  lead:leaders;
  errMss:string;
 @Output()
 home:leader;
  constructor(private leader: LeaderService,@Inject('BaseURL') private baseURL){}
ngOnInit() {
  this.leader.getLeader().subscribe((lead)=>{
  this.lead=lead
  },
   errmess => 
   { 
   this.lead = null;
   this.errMss = errmess; 

  });
  this.leader.getFeatured().subscribe((lead)=>{
  this.home=lead},
   errmess => { this.lead = null; this.errMss = <any>errmess; 

  }) 

  }
  
  }

