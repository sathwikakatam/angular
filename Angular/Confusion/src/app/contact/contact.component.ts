import { Component, OnInit, ViewChild } from '@angular/core';
import { FeedbackformService } from '../Service/feedbackform.service';
import { feedback,Contact_type} from '../Shared/FeedBack';
import {FormBuilder, FormGroup, Validators}  from '@angular/forms';
import { flyInout } from '../Shared/animations';
import { expand, feed,form } from '../Shared/animations';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  host:{
  '[@flyInout]':'true',
  'style':'display=block;'

  },
  animations:[
  flyInout(),
  expand(),
  feed(),
  form()
  ]
})

export class ContactComponent implements OnInit {
  fb:feedback;
  ref_fb:feedback;
  contact=Contact_type;
  feedbackgroup:FormGroup;
  
  fd='';
 

  @ViewChild('fform')feedbackgroupDirective;

  constructor(private fbuilder: FormBuilder, private contact_s:FeedbackformService ) { 
 this.createForm();
  }
  ngOnInit() {
   // this.contact_s.getFeedback().subscribe(feed =>{ this.fb= feed; this.ref_fb =feed;});

  }
  createForm():void{
   this.feedbackgroup=this.fbuilder.group({
   firstName:['',[Validators.required,Validators.maxLength(25),Validators.minLength(2)]],
	 phoneNumber: ['',[Validators.required,Validators.pattern]],
	 email:['',[Validators.required,Validators.email]],
   lastName:['',[Validators.required,Validators.minLength(2),Validators.maxLength(25),]],        
   agree:false,
   contact:'None',
   message:''

 });
 this.feedbackgroup.valueChanges.subscribe((data)=>this.onChanges(data));
 this.onChanges();
 }


 formErrors={
 'firstName':'',
 'lastName':'',
 'phoneNumber':'',
 'email':''

 };
 errormessages={
'firstName':{
  'required':      'First Name is required.',
  'minlength':     'First Name must be at least 2 characters long.',
  'maxlength':     'FirstName cannot be more than 25 characters long.'
},
'lastName':{
'required':      'Last Name is required.',
'minlength':     'Last Name must be at least 2 characters long.',
'maxlength':     'Last Name cannot be more than 25 characters long.'
  
},
'phoneNumber':{
  'required':      'Tel. number is required.',
  'pattern':       'Tel. number must contain only numbers.'
},
'email':{
  'required':      'Email is required.',
  'email':         'Email not in valid format.'
},
};


 onChanges(data?:any){
  if (!this.feedbackgroup) { return; }
  const form = this.feedbackgroup;
  for (const field in this.formErrors) {
   if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
  this.formErrors[field] = '';
  const control = form.get(field);
  if (control && control.dirty && !control.valid) {
  const messages = this.errormessages[field];
  for (const key in control.errors) {
  if (control.errors.hasOwnProperty(key)) {
  this.formErrors[field] += messages[key] + ' ';
  }

    }
    }
  }
 }
 
 }



 onSubmit(){
this.fb=this.feedbackgroup.value;

this.contact_s.addFeedback(this.fb).subscribe(fed =>{ this.ref_fb=fed;});

 this.feedbackgroup.reset({
 F_Name:'',
 Tele:0,
 email:'',
 S_Name:'',
 agree:false,
 message:'',
 contact:'None'
 });
    setTimeout(()=>{
  if(this.ref_fb){
   this.fd="your feed back is great!!";
  this.ref_fb=undefined; 
  
  console.log("after submiting"+this.ref);
  }},5000);
 this.feedbackgroupDirective.resetForm();
 
 }

}
