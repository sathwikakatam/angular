import { Component, OnInit,Inject } from '@angular/core';
import { DishService } from '../Service/dish.service';
import { dishe } from '../Shared/dishes';
import { flyInout,expand } from '../Shared/animations';
@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  host:{
  '[@flyInout]':'true',
  'style':'display=block;'

  },
  animations:[
  flyInout(),
  expand()
  ]
})
export class MenuComponent implements OnInit {
 dishs:dishe[];
 errMsg:string;
 selectedDish:dishe;
  onSelect(dish: dishe) {
    this.selectedDish = dish;
   console.log(this.selectedDish);
    }
 
  constructor( private dish : DishService,@Inject('BaseURL') private baseURL){}
  ngOnInit() {
   this.dish.getDishes().subscribe((dishs)=>this.dishs =dishs, error => this.errMsg=<any>error);
   }

   

}
