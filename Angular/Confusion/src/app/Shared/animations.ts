import { Component, HostBinding } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';



export function visibility() {
    return trigger('visibility', [
        state('shown', style({
            transform: 'scale(1.0)',
            opacity: 1
        })),
        state('hidden', style({
            transform: 'scale(0.5)',
            opacity: 0
        })),
        transition('* => *', animate('0.5s ease-in-out'))
    ]);
}

export function flyInout(){
	return trigger('flyInout',[
         state('*', style({
                opacity:1,
                transform:'translateX(0)'
         })),
         transition(':enter',[
           style({
           opacity:0,
           transform:'translateX(-100%)'
           }),
           animate('500ms ease-in')

         ]),
         transition(':leave',[
         animate('500ms ease-out',
         style({
           opacity:1,
           transform:'translateX(100%)'
         })),
         

         ])


	]);
}

export function feed(){
  return trigger('feed',[
         state('*', style({
                opacity:1,
                transform:'translateX(0)'
         })),
         transition(':enter',[
           style({
           opacity:0,
           transform:'translateX(0)'
           }),
           animate('500ms ease-in')

         ]),
         transition(':leave',[
         animate('4s ease-out',
         style({
           opacity:1,
           
           transform:'translateX(0)'
         })),
         

         ])


  ]);
}
export function form(){
  return trigger('form',[
         state('*', style({
                opacity:1,
                transform:'translateX(0)'
         })),
         transition(':enter',[
           style({
           opacity:0,
           transform:'translateX(0)'
           }),
           animate('4s ease-in')

         ]),
         transition(':leave',[
         animate('0s ease-out',
         style({
           opacity:0,
           
           transform:'translateX(0)'
         })),
         

         ])


  ]); 
}



export function expand(){
  return trigger('expand',[
          state('*',style({
              opacity:1,
              transform:'translateX(0)'
          })),
          transition(':enter',[
           style({
           opacity:0,
           transform:'translateX(-50%)'
           }),
           animate('1.0s ease-in')
          ])


  ]);
}