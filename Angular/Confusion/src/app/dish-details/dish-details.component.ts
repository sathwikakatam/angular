import { Component, OnInit,Input,Inject } from '@angular/core';
//import {DISHES } from '../Shared/dishesCommon';
import {dishe} from '../Shared/dishes';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DishService } from '../Service/dish.service';
import { switchMap } from 'rxjs/operators';
import { FormGroup, FormControl,Validators,FormBuilder } from '@angular/forms';
import { Comment } from '../Shared/Comment';
import { visibility } from '../Shared/animations';
import { flyInout ,expand} from '../Shared/animations';



@Component({
  selector: 'app-dish-details',
  templateUrl: './dish-details.component.html',
  styleUrls: ['./dish-details.component.scss'],
  host:{
  '[@flyInout]':'true',
  'style':'display=block;'

  },
  animations:[
  flyInout(),
  visibility(),
  expand()
  ]
})
export class DishDetailsComponent implements OnInit {
   dish:dishe;
   disheId:string[];
   comment:Comment;
   next:string;
   prev:string;
   dishcopy:string;
   errMss:string;
   visibility='shown';
   
   comments_Form:FormGroup;

  constructor(private dishservice:DishService ,private route: ActivatedRoute,
  private location:Location,private fb:FormBuilder, @Inject('BaseURL') private baseURL){ 
  }

  ngOnInit() {
  //this.data.P_rating=5;
  this.dishservice.getDishIds().subscribe((id)=>this.disheId=id);
  this.route.params.pipe(switchMap((params:Params)=> 
                           {
                          this.visibility='hidden';
                          return  this.dishservice.getDish(params['id']);
                          }))
                          .subscribe(dish=>{
                                             
                                              this.dish=dish;
                                              this.dishcopy = dish;
                                              this.setPrvNext(dish.id);
                                               this.visibility='shown';

                                              });
                                                
    this.creatform();
                                              
                          

 }

  creatform(){
    this.comments_Form=this.fb.group({
    "author":['',[Validators.required,Validators.minLength(2),Validators.maxLength(20)]],
    "comment":['',[Validators.required]],
   "rating":['5',[Validators.required]]

    });
   this.onValueChanges();
  }
   
  onValueChanges(){
  this.comments_Form.valueChanges.subscribe(data=> {
  this.onchanges(data);
  });
  this.onchanges();
  }
  


  onSubmit(){
   this.comment=this.comments_Form.value;
   this.comment.date= (new Date()).toString();;
   console.log(this.comment);
   this.dishcopy.comments.push(this.comment);

  
this.dishservice.PutDish(this.dishcopy).subscribe(dish=>{
//this.dish=dish;
  //this.dishcopy=dish;
//this.data.push(this.date);

 },

 errmess => { this.dish = null; this.dishcopy = null; this.errMss = <any>errmess; 
   }
   );
   this.onrest();
  }
  
onrest(){
 

 this.comments_Form.reset({
      author:'',
      comment: '',
      rating: '5'
    });
}


onchanges(data?:any){
if (!this.comments_Form) { return; }
  const form = this.comments_Form;
  for (const field in this.formErrors) {
   if (this.formErrors.hasOwnProperty(field)) {
        // clear previous error message (if any)
  this.formErrors[field] = '';
  const control = form.get(field);
  if (control && control.dirty && !control.valid) {
  const messages = this.errormessages[field];
  //console.log(messages);
  for (const key in control.errors) {
  if (control.errors.hasOwnProperty(key)) {
  this.formErrors[field] += messages[key] + ' ';
  }
 }
}
}
}
}




  formErrors={
    "author":" ",
    "comments":" ",
    "rating":" "
  }

  errormessages={
    "author":{
   "required":      "author is required.",
   "minlength":     "author must be at least 2 characters long.",
   "maxlength":     "author cannot be more than 20 characters long."
  },
 "comments":{
     "required":  "comments are requied"

  },
 "ratings":{
    "required": "rating are required"
  }
}



  setPrvNext(id:string){

 const index=this.disheId.indexOf(id);

 this.next=this.disheId[(this.disheId.length+index+1)%(this.disheId.length)];

 this.prev=this.disheId[(index+this.disheId.length-1)%(this.disheId.length)];
 }

 goBack(): void {
    this.location.back();
}

}
