import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProcessHttpMsgService {
errmsg:string;
  constructor() { }
  private handleError(error: HttpErrorResponse) {
  if (error.error instanceof ErrorEvent) {
    this.errmsg='An error occurred:', error.error.message;
  } 
  else {
   
   this.errmsg= `Backend returned code ${error.status}, `+`body was: ${error.error}`
  }
  return throwError(this.errmsg);
};
}
