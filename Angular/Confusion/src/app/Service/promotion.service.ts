import { Injectable } from '@angular/core';
import { promotions } from '../Shared/Promotion';
//import { PROMOTIONS } from '../Shared/promotionsData';
//import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { baseURL } from '../Shared/BaseUrl';
import { of } from 'rxjs';
import {delay,map} from 'rxjs/operators';
import { ProcessHttpMsgService  } from './process-http-msg.service';
import { catchError, retry } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PromotionService {

constructor( private http: HttpClient,private error:ProcessHttpMsgService){ }

  getPromotions():Observable<promotions[]> {
    return this.http.get<promotions[]>(baseURL+"promotions").pipe(catchError(this.error.handleError));
    }
  getPromotion(id: string): Observable<promotions> {
    return this.http.get<promotions>(baseURL+'promotions/'+id).pipe(catchError(this.error.handleError));
  }
getFeaturedPromotion(): Observable<promotions> {
    return  this.http.get<promotions[]>(baseURL+'promotions?featured=true').pipe(map(promotion=>promotion[0])).pipe(catchError(this.error.handleError));
  
 
}
}
