import { TestBed } from '@angular/core/testing';

import { FeedbackformService } from './feedbackform.service';

describe('FeedbackformService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FeedbackformService = TestBed.get(FeedbackformService);
    expect(service).toBeTruthy();
  });
});
