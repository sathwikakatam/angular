import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { feedback } from '../Shared/FeedBack';
import { baseURL } from '../Shared/BaseUrl';
import { ProcessHttpMsgService } from './process-http-msg.service';
import { catchError, retry } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})

@Injectable({
  providedIn: 'root'
})
export class FeedbackformService {
Feedback:feedback;

  constructor(private http: HttpClient,private error : ProcessHttpMsgService) { }
  const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
   // 'Authorization': 'my-auth-token'
  })
};
 

addFeedback (Feedback:feedback): Observable<feedback> {
  return this.http.post<feedback>(baseURL+"feedbacks", Feedback, this.httpOptions)
    .pipe(
      catchError(this.error.handleError('addFeedback',Feedback))
    );
}

  
}
