import { Injectable } from '@angular/core';
import { leaders} from '../Shared/leader';
import{  baseURL } from '../Shared/BaseUrl';
import { of } from 'rxjs';
import {delay,map} from 'rxjs/operators';
import { catchError, retry } from 'rxjs/operators';
import { ProcessHttpMsgService  } from './process-http-msg.service';

import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  constructor(private http :HttpClient,private error: ProcessHttpMsgService) { }
  


  getLeader(): Observable<leaders>{
    
  return this.http.get<leaders[]>(baseURL+"leaders").pipe(catchError(this.error.handleError));
  
  }
  getFeatured(): Observable<leaders>{

  return this.http.get<leaders[]>(baseURL+"leaders?featured=true").pipe(map(leaders=>leaders[0])).pipe(catchError(this.error.handleError));
  
}
}

