import { Injectable } from '@angular/core';
import { dishe } from '../Shared/dishes';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { delay,map} from 'rxjs/operators';
import { baseURL } from '../shared/BaseUrl';
import { of } from 'rxjs';
import { ProcessHttpMsgService  } from './process-http-msg.service';
import { catchError, retry } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DishService {

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
    //'Authorization': 'my-auth-token'
  })
};


  constructor(private http: HttpClient,private error:ProcessHttpMsgService) { }

  getDishes(): Observable<dishe[]> {
       return this.http.get<dishe[]>(baseURL+"dishes").  pipe(catchError(this.error.handleError));

       
  } 
  getDish(id:string): Observable<dishe>{
   return  this.http.get<dishe>(baseURL + "dishes/" + id).pipe(catchError(this.error.handleError));

  }
  getFeaturedDish():Observable<dishe>{
  return  this.http.get<dishe[]>(baseURL + 'dishes?featured=true').pipe(map(dishes => dishes[0])).
  pipe(catchError(this.error.handleError));
    }
 
 getDishIds():Observable<string[]|any>{
     return this.getDishes().pipe(map(dishes => dishes.map(dish =>dish.id))).pipe(catchError(this.error.handleError));
 }

  PutDish(dishs: dishe ): Observable<Hero> {
  
  return this.http.put<dishe>(baseURL+"dishes/"+dishs.id,dishs,this.httpOptions)
    .pipe(
      catchError(this.error.handleError)
    );
}


}
