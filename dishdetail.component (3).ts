import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/form
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Dish } from '../shared/dish';
import { DishService } from '../services/dish.service';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})
export class DishdetailComponent implements OnInit {

  @Input()
  dishdetailForm: FormGroup;
  comment: Comment;
  array_copy = null;
  dish: Dish;
  dishIds: number[];
  prev: number;
  next: number;
  errorMessage: string;
  
  formErrors = {
    'author': '',
    'comment': ''
  };
  
  validationMessages = {
    'author': {
      'required': 'Author is required.',
      'minlength': 'Author name must be at least 2 characters long.',
      'maxlength': 'Author name cannot be more than 25 characters long.'
    },
    'comment': {
      'required': 'Comment is required.',
      'minlength': 'Comment must be at least 2 characters long.'
    },
  };

  constructor(private dishService: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fb: FormBuilder) { 
      this.createForm();
  }

  ngOnInit() {
    this.dishdetailForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)] ],
      comment: ['', [Validators.required, Validators.minLength(2)]],
      rating: ['5'],
      date: ['']
    });
    
    this.dishdetailForm.valueChanges
      .subscribe(data => this.onValueChanged(data));
    
    this.onValueChanged(); //Reset form validation messages
  }
  
  onValueChanged(data?: any) {
    if (!this.dishdetailForm) { return; }
    const form = this.dishdetailForm;
    
    for (const field in this.formErrors) {
      this.formErrors[field] = '';
      const control = form.get(field);
      if (control && control.dirty && !control.valid) {
        const messages = this.validationMessages[field];
        for (const key in control.errors) {
          this.formErrors[field] += messages[key] + ' ';
        }
      }
    }
  }
  
  onSubmit() {
    this.comment = this.dishdetailForm.value;
    this.comment.date = (new Date()).toString();
    this.array_copy.comments.push(this.comment);
    this.dishdetailForm.reset({
      author: '',
      comment: '',
      rating: '5'
    });
  }
  
  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];

  goBack(): void {
    this.location.back();
  }

}
